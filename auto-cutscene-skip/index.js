'use strict';
String.prototype.clr = function(hexColor) {
    return `<font color='#${hexColor}'>${this}</font>`;
};
const config = require('./config.json');

module.exports = function AutoCutscene(mod) {
    const cmd = mod.command || mod.require.command

    let enable = config.enable;

    cmd.add('skip', {
        $none() {
            enable = !enable;
            mod.command.message('Модуль: ' + (s.enable ? 'Включен'.clr('56B4E9') : 'Отключен'.clr('FF0000')) + '.');
        }
    });

    mod.hook('S_PLAY_MOVIE', 1, (e) => {
        if (!enable) return
        m.send('C_END_MOVIE', 1, Object.assign({
            unk: 1
        }, e));
        return false
    });

    function send(msg) {
        cmd.message(`: ` + msg);
    }

}