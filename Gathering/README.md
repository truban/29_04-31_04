# Подсказки для сбора ресурсов
======
<p align="center"><img src="https://gitlab.com/truban/29_04-31_04/-/raw/main/Gathering/screen/bg.png" alt="Your image title" width="1000"/>


## Описание:
Подсветка рессурсов с выводом сообщений, как всех, так и по отдельности.

------

Комманда | Описание
| ------------- | ------------- |
| gat | Включение модуля
| gat ui | Включение GUI модуля
| gat alert | Уведомление в центре
| gat plants | Вкл/Выкл - обнаружение (подсветка) растениям
| gat ore | Вкл/Выкл - обнаружение (подсветка)о руде
| gat energy | Вкл/Выкл - обнаружение (подсветка) энергии
| gat status | Состояние модуля

------
*Предупреждающее сообщение (Alert) `warn`*
<p align="center"><img src="https://gitlab.com/truban/29_04-31_04/-/raw/main/Gathering/screen/1.png"/>

*Уведомление (Notice) `not`*
<p align="center"><img src="https://gitlab.com/truban/29_04-31_04/-/raw/main/Gathering/screen/2.png"/>

------

### Команды интегрированы в модуль прокси-меню

### Commands are integrated into the proxy menu module

------

## Модуль работает только на классической версии Tera

