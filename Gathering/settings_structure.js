module.exports = [
    {
        "key": "enabled",
        "name": "Включение/Отключение Модуля",
        "type": "bool"
    },
    {
        "key": "sendToAlert",
        "name": "Уведомление в центре",
        "type": "bool"
    },
    {
        "key": "plantsMarkers",
        "name": "Подсказки по растениям",
        "type": "bool"
    },
    {
        "key": "miningMarkers",
        "name": "Подсказки по руде",
        "type": "bool"
    },
    {
        "key": "energyMarkers",
        "name": "Подсказки по энергии",
        "type": "bool"
    }
]
