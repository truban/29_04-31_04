// Item IDs that have unique glows
//
// 91116   Amarun's Relic Piece     //Avatar
// 55658   Bahaar's Relic Piece     //Avatar
// 91166   Dagon's Relic Piece      //Avatar
// 91114   Elinu's Relic Piece      //Avatar
// 91118   Gidd's Relic Piece       //Avatar
// 91177   Ishara's Relic Piece     //Avatar
// 91113   Isren's Relic Piece      //Avatar
// 91119   Karas's Relic Piece      //Avatar
// 91188   Oriyn's Relic Piece      //Avatar
// 57000   Seren's Relic Piece      //Avatar
// 91115   Tithus's Relic Piece     //Avatar
// 91117   Zuras's Relic Piece      //Avatar
// 98260   Vergos's Head            // Vergo pieces all have the same effect
// 98263   Vergos's Horn
// 98262   Vergos's Scale
// 98264   Vergos's Bone
// 98261   Vergos's Fang
// 440 649 6515 8000-8024 45411 91113
// 88704 // Памятная монета Велики
String.prototype.clr = function(hexColor) {
    return `<font color='#${hexColor}'>${this}</font>`;
};

const SettingsUI = require('tera-mod-ui').Settings;

module.exports = function Gathering(mod) {

    if (mod.proxyAuthor !== '') {
        const options = require('./module').options
        if (options) {
            const settingsVersion = options.settingsVersion
            if (settingsVersion) {
                mod.settings = require('./' + (options.settingsMigrator || 'settings_migrator.js'))(mod.settings._version, settingsVersion, mod.settings)
                mod.settings._version = settingsVersion
            }
        }
    }

    let mobid = [],
        gatherMarker = [];

    // Settings UI
    let ui = null;
    if (global.TeraProxy.GUIMode) {
        ui = new SettingsUI(mod, require('./settings_structure'), mod.settings, {
            "width": 400,
            "height": 230,
            "resizable": false
        });
        ui.on("update", settings => {
            mod.settings = settings;
        });
    }

    function gatheringStatus() {
        sendStatus("",
            mod.command.message('Модуль: ' + (mod.settings.enabled ? 'Включен'.clr('56B4E9') : 'Отключен'.clr('FF0000'))),
            mod.command.message('Уведомление в центре: ' + (mod.settings.sendToAlert ? 'Включены'.clr('56B4E9') : 'Отключены'.clr('FF0000'))),

            mod.command.message('Подсказки по растениям: ' + (mod.settings.plantsMarkers ? 'Включены'.clr('56B4E9') : 'Отключены'.clr('FF0000'))),
            mod.command.message('Подсказки по руде: ' + (mod.settings.miningMarkers ? 'Включены'.clr('56B4E9') : 'Отключены'.clr('FF0000'))),
            mod.command.message('Подсказки по энергии: ' + (mod.settings.energyMarkers ? 'Включены'.clr('56B4E9') : 'Отключены'.clr('FF0000')))
        );
    }

    function sendStatus(msg) {
        sendMessage([...arguments].join('\n\t'));
    }

    mod.command.add("gat", (arg) => {
        if (!arg) {
            mod.settings.enabled = !mod.settings.enabled;
            if (!mod.settings.enabled) {
                for (let itemId of mobid) {
                    despawnItem(itemId);
                }
            }
            gatheringStatus();
        } else {
            switch (arg) {
                case "set":
                    if (ui) ui.show();
                    break
                case "alert":
                    mod.settings.sendToAlert = !mod.settings.sendToAlert;
                    sendMessage('Уведомление в центре: ' + (mod.settings.sendToAlert ? 'Включено'.clr('56B4E9') : 'Отключено'.clr('FF0000')));
                    break
                case "status":
                    gatheringStatus();
                    break
                case "plants":
                    mod.settings.plantsMarkers = !mod.settings.plantsMarkers;
                    sendMessage('Подсказки по растениям : ' + (mod.settings.plantsMarkers ? 'Показаны'.clr('56B4E9') : 'Скрыты'.clr('FF0000')));
                    break
                case "ore":
                    mod.settings.miningMarkers = !mod.settings.miningMarkers;
                    sendMessage('Подсказки по руде : ' + (mod.settings.miningMarkers ? 'Показаны'.clr('56B4E9') : 'Скрыты'.clr('FF0000')));
                    break
                case "energy":
                    mod.settings.energyMarkers = !mod.settings.energyMarkers;
                    sendMessage('Подсказки по энергии : ' + (mod.settings.energyMarkers ? 'Показаны'.clr('56B4E9') : 'Скрыты'.clr('FF0000')));
                    break
                default:
                    sendStatus("",
                        ('gat: ' + ('Включение модуля'.clr('56B4E9'))),
                        ('gat ui: ' + ('Включение GUI модуля'.clr('56B4E9'))),
                        ('gat alert: ' + ('Уведомление в центре'.clr('56B4E9'))),
                        ('gat plants: ' + ('Включает Подсказки по растениям'.clr('56B4E9'))),
                        ('gat ore: ' + ('Включает Подсказки по руде'.clr('56B4E9'))),
                        ('gat energy: ' + ('Включает Подсказки по энергии'.clr('56B4E9'))),
						('gat status: ' + ('Состояние модуля'.clr('56B4E9')))
                    );
                    break
            }
        }
    })

    mod.game.me.on('change_zone', (zone, quick) => {
        mobid = [];
    })

    mod.hook('S_SPAWN_COLLECTION', 4, (event) => {
        if (mod.settings.enabled) {
            if (mod.settings.plantsMarkers && (gatherMarker = mod.settings.plants.find(obj => obj.id === event.id))) {
                if (mod.settings.sendToAlert) {
                    sendAlert(('Обнаружено: ').clr('FF8C00') + (`${[gatherMarker.name]} -- `).clr('00FF00') + (`${gatherMarker.msg}`).clr('32CD32'));
                    sendMessage('Обнаружено: '.clr('FF8C00') + (`${[gatherMarker.name]} `).clr('00FF00') + (`${gatherMarker.msg}`).clr('32CD32'));
                }
            } else if (mod.settings.miningMarkers && (gatherMarker = mod.settings.mining.find(obj => obj.id === event.id))) {
                if (mod.settings.sendToAlert) {
                    sendAlert(('Обнаружено: ').clr('FF8C00') + (`${[gatherMarker.name]} -- `).clr('FF6347') + (`${gatherMarker.msg}`).clr('FF4500'));
                    sendMessage('Обнаружено: '.clr('FF8C00') + (`${[gatherMarker.name]} `).clr('FF6347') + (`${gatherMarker.msg}`).clr('FF4500'));
                }
            } else if (mod.settings.energyMarkers && (gatherMarker = mod.settings.energy.find(obj => obj.id === event.id))) {
                if (mod.settings.sendToAlert) {
                    sendAlert(('Обнаружено: ').clr('FF8C00') + (`${[gatherMarker.name]} -- `).clr('00BFFF') + (`${gatherMarker.msg}`).clr('1E90FF'));
                    sendMessage('Обнаружено: '.clr('FF8C00') + (`${[gatherMarker.name]} `).clr('00BFFF') + (`${gatherMarker.msg}`).clr('1E90FF'));
                }
            } else {
                return true;
            }

            spawnItem(event.gameId, event.loc);
            mobid.push(event.gameId);
        }
    })

    mod.hook('S_DESPAWN_COLLECTION', 2, (event) => {
        if (mobid.includes(event.gameId)) {
            gatherMarker = [];
            despawnItem(event.gameId);
            mobid.splice(mobid.indexOf(event.gameId), 1);
        }
    })

    function spawnItem(gameId, loc) {
        loc.z -= 100;
        mod.send('S_SPAWN_DROPITEM', 6, {
            gameId: gameId * 100n,
            loc: loc,
            item: mod.settings.markerId,
            amount: 1,
            expiry: 0,
            owners: []
        });
    }

    function despawnItem(gameId) {
        mod.send('S_DESPAWN_DROPITEM', 4, {
            gameId: gameId * 100n
        });
    }

    function sendMessage(msg) {
        mod.command.message(msg)
    }

    function sendAlert(msg) {
        mod.send('S_CHAT', 2, {
            channel: 21,
            chat: false,
            message: msg,
        });
    }

    this.destructor = () => {
        if (ui) {
            ui.close();
            ui = null;
        }
        mod.command.remove("gat");
    };
}