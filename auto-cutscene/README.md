### auto-cutscene
Автоматически пропускает все видеоролики

## Dependency
- `command` module

## Usage
- __`skip`__
  - Toggle on/off
  
- `cutscene <num>`
  - Play cutscene `<num>`  

## Config
- __`enable`__
  - Initialize module on/off
  - Default is `true`

## Info
- Original author : [baldera-mods](https://github.com/baldera-mods)

## Changelog
<details>

</details>
