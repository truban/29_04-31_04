'use strict';
String.prototype.clr = function(hexColor) {
	return `<font color='#${hexColor}'>${this}</font>`;
};

class auto_cutscene {

	constructor(mod) {

		this.command = mod.command;

		if (mod.majorPatchVersion >= 105)
			return mod.warn('Deprecated. please refer to the in-game option to toggle cutscene.');

		// command
		mod.command.add('skip', {
			'$default': () => {
				mod.settings.enable = !mod.settings.enable;
				mod.command.message('Модуль: ' + (mod.settings.enable ? 'Включен'.clr('56B4E9') : 'Отключен'.clr('FF0000')));
			}
		});

		mod.command.add('cutscene', {
			'$default': (num) => {
				if (num && !isNaN(num)) {
					mod.send('S_PLAY_MOVIE', 1, {
						movie: num
					});
					mod.command.message('Попытка воспроизвести кат-сцену '.clr('E0FFFF') + (`${num} `).clr('DC143C'));
				}
			}
		});

		// code
		mod.hook('S_PLAY_MOVIE', 1, (e) => {
			if (mod.settings.enable) {
				mod.send('C_END_MOVIE', 1, Object.assign({
					unk: 1
				}, e));
				return false;
			}
		});

	}

	destructor() {
		this.command.remove('skip');
		this.command.remove('cutscene');
	}

	send(msg) {
		this.command.message(': ' + msg);
	}

}

module.exports = {
	NetworkMod: auto_cutscene
};