'use strict';
String.prototype.clr = function(hexColor) {
    return `<font color='#${hexColor}'>${this}</font>`;
};
module.exports = function antiCC(mod) {
	let location = null;
	let locationTime = 0;

	mod.command.add("cc", () => {
		mod.settings.enabled = !mod.settings.enabled;
		mod.command.message('Модуль: ' + (mod.settings.enabled ? 'Включен'.clr('56B4E9') : 'Отключен'.clr('FF0000')) + '.');
	});

	mod.game.me.on("change_zone", () => {
		if (!mod.game.me.inOpenWorld) {
			mod.command.message('Модуль: ' + (mod.settings.enabled ? 'Включен'.clr('56B4E9') : 'Отключен'.clr('FF0000')) + '.');
		}
	});

	mod.hook("C_PLAYER_LOCATION", 5, { "order": Infinity }, event => {
		location = event;
		locationTime = Date.now();
	});

	mod.hook("S_EACH_SKILL_RESULT", 13, { "order": -Infinity }, event => {
		if (!mod.settings.enabled) return;

		if (mod.game.me.is(event.target) && event.reaction.enable) {
			if (!event.reaction.push) {
				mod.send("C_PLAYER_LOCATION", 5, {
					...location,
					"type": 2,
					"time": location.time - locationTime + Date.now() - 50
				});

				mod.send("C_PLAYER_LOCATION", 5, {
					...location,
					"type": 7,
					"time": location.time - locationTime + Date.now() + 50
				});
			}

			Object.assign(event.reaction, {
				"enable": false,
				"push": false,
				"air": false,
				"airChain": false
			});

			return true;
		}
	});
};