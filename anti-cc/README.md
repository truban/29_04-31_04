# anti-cc

Модуль для предотвращения того, чтобы вас не сбили с ног в PVP или PVE

## Commands

Toolbox(/8) | Command description
--- | ---
**cc** | Module on/off.

This version was forked from the original module. Credits go to the author "Busann".
