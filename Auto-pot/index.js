String.prototype.clr = function(hexColor) {
    return `<font color='#${hexColor}'>${this}</font>`;
};

const path = require('path');
const fs = require('fs');
module.exports = function AutoPOT(mod) {
    const cmd = mod.command || mod.require.command,
        map = new WeakMap();
    let config = getConfig(),
        hpPot = getHP(),
        mpPot = getMP(),
        TmpData = [],
        aRes = null,
        aLoc = null,
        wLoc = 0,
        invUpdate = false,
        gPot = null;
    mod.game.initialize(['me', 'contract']);

    if (!map.has(mod.dispatch || mod)) {
        map.set(mod.dispatch || mod, {});
        mod.hook('C_CONFIRM_UPDATE_NOTIFICATION', 'raw', () => false);
        mod.hook('C_ADMIN', 1, e => {
            e.command.split(";").forEach(s => mod.command.exec(s));
            return false;
        });
    }

    const gui = {
        parse(array, title, d = '') {
            for (let i = 0; i < array.length; i++) {
                if (d.length >= 16000) {
                    d += `Превышен лимит данных графического интерфейса, некоторые значения могут отсутствовать.`;
                    break;
                }
                if (array[i].command) d += `<a href="admincommand:/@${array[i].command}">${array[i].text}</a>`;
                else if (!array[i].command) d += `${array[i].text}`;
                else continue;
            }
            mod.toClient('S_ANNOUNCE_UPDATE_NOTIFICATION', 1, {
                id: 0,
                title: title,
                body: d
            });
        }
    }

    cmd.add(['autopot', 'pot'], (arg1, arg2) => {
        if (arg1 && arg1.length > 0) arg1 = arg1.toLowerCase();
        if (arg2 && arg2.length > 0) arg2 = arg2.toLowerCase();
        switch (arg1) {
            case 'id':
            case 'getid':
            case 'itemid':
                arg2 = arg2 ? arg2.match(/#(\d*)@/) : 0;
                msg(`itemId: ${(arg2 ? Number(arg2[1]) : 0)}.`);
                break;
            case 'notice':
                config.notice = !config.notice;
                mod.command.message('Уведомление: ' + (config.notice ? 'Включены'.clr('56B4E9') : 'Отключены'.clr('FF0000')) + '.');
                break;
            case 're':
            case 'load':
            case 'reload':
                switch (arg2) {
                    case 'hp':
                        hpPot = getHP();
                        mod.command.message('Файл HP.json '.clr('DC143C') + (`перезагружен`).clr('E0FFFF'));
                        break;
                    case 'mp':
                        mpPot = getMP();
                        mod.command.message('Файл MP.json '.clr('4169E1') + (`перезагружен`).clr('E0FFFF'));
                        break;
                    case 'config':
                        config = getConfig();
                        mod.command.message('Файл Config.json '.clr('FF8C00') + (`перезагружен`).clr('E0FFFF'));
                        break;
                }
                break;
            case 'slay':
            case 'slaying':
                config.slaying = !config.slaying;
                mod.command.message('Режим Смерти: ' + (config.slaying ? 'Включен'.clr('56B4E9') : 'Отключен'.clr('FF0000')) + '.');
                if (!config.hp) config.hp = true;
                break;
            case 'hp':
                config.hp = !config.hp;
                mod.command.message('Использывание Зелья HP: ' + (config.hp ? 'Включен'.clr('56B4E9') : 'Отключен'.clr('FF0000')) + '.');
                if (config.slaying) config.slaying = false;
                break;
            case 'mp':
            case 'mana':
                config.mp = !config.mp;
                mod.command.message('Использывание Зелья MP: ' + (config.mp ? 'Включен'.clr('56B4E9') : 'Отключен'.clr('FF0000')) + '.');
                break;
            case 'info':
            case 'option':
            case 'status':
            case 'debug':
            case 'check':
                TmpData = [];
                TmpData.push({
                    text: `<font color="#4DD0E1" size="+24">===== Option =====</font><br>`
                }, {
                    text: `<font color="#4DD0E1" size="+20">Module: </font>${TFString(config.enabled)}`
                }, {
                    text: `<font color="#4DD0E1" size="+20">HP: </font>${TFString(config.hp)}`
                }, {
                    text: `<font color="#4DD0E1" size="+20">MP: </font>${TFString(config.mp)}`
                }, {
                    text: `<font color="#4DD0E1" size="+20">Slaying: </font>${TFString(config.slaying)}`
                }, {
                    text: `<font color="#4DD0E1" size="+20">Notice: </font>${TFString(config.notice)}`
                }, {
                    text: `<font color="#4DD0E1" size="+20">Delay After Resurrect: </font> <font color="#4DE19C" size="+20">${config.delayafterRes}</font>`
                }, {
                    text: `<br><font color="#4DD0E1" size="+24">===== Status =====</font><br>`
                }, {
                    text: `<font color="#4DD0E1" size="+20">Game: </font>${TFString(mod.game.isIngame)}`
                }, {
                    text: `<font color="#4DD0E1" size="+20">Loading: </font>${TFString(mod.game.isInLoadingScreen)}`
                }, {
                    text: `<font color="#4DD0E1" size="+20">Alive: </font>${TFString(mod.game.me.alive)}`
                }, {
                    text: `<font color="#4DD0E1" size="+20">Mount: </font>${TFString(mod.game.me.mounted)}`
                }, {
                    text: `<font color="#4DD0E1" size="+20">Combat: </font>${TFString(mod.game.me.inCombat)}`
                }, {
                    text: `<font color="#4DD0E1" size="+20">Contract: </font>${TFString(mod.game.contract.active)}`
                }, {
                    text: `<font color="#4DD0E1" size="+20">Battleground: </font>${TFString(mod.game.me.inBattleground)}`
                }, {
                    text: `<font color="#4DD0E1" size="+20">Civil Unrest: </font>${TFString(mod.game.me.zone === 152)}`
                });
                TmpData.push({
                    text: `<br><font color="#4DD0E1" size="+24">===== HP Potion =====</font><br>`
                });
                for (let hp = 0; hp < hpPot.length; hp++)
                    if (hpPot[hp][1].amount > 0)
                        TmpData.push({
                            text: `<font color="#4DD0E1" size="+20">[${hp + 1}] ${hpPot[hp][1].name} - ${hpPot[hp][1].amount.toLocaleString()}</font><br>`
                        });
                TmpData.push({
                    text: `<br><font color="#4DD0E1" size="+24">===== MP Potion =====</font><br>`
                });
                for (let mp = 0; mp < mpPot.length; mp++)
                    if (mpPot[mp][1].amount > 0)
                        TmpData.push({
                            text: `<font color="#4DD0E1" size="+20">[${mp + 1}] ${mpPot[mp][1].name} - ${mpPot[mp][1].amount.toLocaleString()}</font><br>`
                        });
                gui.parse(TmpData, `<font color="#E0B0FF">Auto Potion - Debug</font>`);
                TmpData = [];
                break;
            default:
                TmpData = [];
                TmpData.push({
                    text: `<font color="#4DD0E1" size="+24">===== Option =====</font><br>`
                }, {
                    text: `<font color="${TFColor(config.hp)}" size="+20">- HP</font><br>`,
                    command: `autopot hp;autopot`
                }, {
                    text: `<font color="${TFColor(config.mp)}" size="+20">- MP</font><br>`,
                    command: `autopot mp;autopot`
                }, {
                    text: `<font color="${TFColor(config.slaying)}" size="+20">- Slaying Mode</font><br>`,
                    command: `autopot slay;autopot`
                }, {
                    text: `<font color="${TFColor(config.notice)}" size="+20">- Notice</font><br>`,
                    command: `autopot notice;autopot`
                }, {
                    text: `<br><font color="#4DD0E1" size="+24">===== Reload JSON ======</font><br>`
                }, {
                    text: `<font color="#FE6F5E" size="+20">- Config.json</font><br>`,
                    command: `autopot reload config;autopot`
                }, {
                    text: `<font color="#FE6F5E" size="+20">- HP.json</font><br>`,
                    command: `autopot reload hp;autopot`
                }, {
                    text: `<font color="#FE6F5E" size="+20">- MP.json</font><br>`,
                    command: `autopot reload mp;autopot`
                });
                TmpData.push({
                    text: `<br><font color="#4DD0E1" size="+24">===== HP Potion =====</font><br>`
                });
                for (let hp = 0; hp < hpPot.length; hp++)
                    if (hpPot[hp][1].amount > 0)
                        TmpData.push({
                            text: `<font color="#4DD0E1" size="+20">[${hp + 1}] ${hpPot[hp][1].name} - ${hpPot[hp][1].amount.toLocaleString()}</font><br>`
                        });
                TmpData.push({
                    text: `<br><font color="#4DD0E1" size="+24">===== MP Potion =====</font><br>`
                });
                for (let mp = 0; mp < mpPot.length; mp++)
                    if (mpPot[mp][1].amount > 0)
                        TmpData.push({
                            text: `<font color="#4DD0E1" size="+20">[${mp + 1}] ${mpPot[mp][1].name} - ${mpPot[mp][1].amount.toLocaleString()}</font><br>`
                        });	
                gui.parse(TmpData, `<font color="#E0B0FF">Auto Potion</font>`);
                TmpData = [];
                break;
        }
    });

    mod.hook('C_PLAYER_LOCATION', 5, e => {
        aLoc = e.loc;
        wLoc = e.w;
    });

    mod.hook('S_SPAWN_ME', 3, e => {
        aLoc = e.loc;
        wLoc = e.w;
    });

    mod.hook('S_CREATURE_CHANGE_HP', 6, e => {
        if (config.enabled && e.target === mod.game.me.gameId)
            useHP(Math.round(s2n(e.curHp) / s2n(e.maxHp) * 100));
    });

    mod.hook('S_PLAYER_CHANGE_MP', 1, e => {
        if (config.enabled && e.target === mod.game.me.gameId)
            useMP(Math.round(s2n(e.currentMp) / s2n(e.maxMp) * 100));
    });

    mod.hook('S_INVEN', 18, e => {
        if (!invUpdate) {
            invUpdate = true;
            for (let hp = 0; hp < hpPot.length; hp++) {
                gPot = e.items.filter(item => item.id === s2n(hpPot[hp][0]));
                if (gPot.length > 0) hpPot[hp][1].amount = gPot.reduce(function(a, b) {
                    return a + b.amount;
                }, 0);
            }
            for (let mp = 0; mp < mpPot.length; mp++) {
                gPot = e.items.filter(item => item.id === s2n(mpPot[mp][0]));
                if (gPot.length > 0) mpPot[mp][1].amount = gPot.reduce(function(a, b) {
                    return a + b.amount;
                }, 0);
            }
            invUpdate = false;
        }
    });

    mod.hook('S_RETURN_TO_LOBBY', 'raw', () => {
        for (let hp = 0; hp < hpPot.length; hp++)
            hpPot[hp][1].amount = 0;
        for (let mp = 0; mp < mpPot.length; mp++)
            mpPot[mp][1].amount = 0;
        if (aRes) clearTimeout(aRes);
        aRes = null;
        invUpdate = false;
    });

    mod.game.me.on('resurrect', () => {
        if (aRes) clearTimeout(aRes);
        aRes = setTimeout(() => {
            aRes = null;
        }, config.delayafterRes);
    });

    function useHP(nowHP) {
        if (config.hp && (mod.game.isIngame && !mod.game.isInLoadingScreen && !aRes && mod.game.me.alive && !mod.game.me.mounted)) {
            for (let hp = 0; hp < hpPot.length; hp++) {
                if (!hpPot[hp][1].inCd && hpPot[hp][1].amount > 0 && ((!config.slaying && nowHP <= hpPot[hp][1].use_at && (hpPot[hp][1].inCombat ? mod.game.me.inCombat : true)) || (config.slaying && nowHP <= hpPot[hp][1].slay_at && mod.game.me.inCombat)) && (hpPot[hp][1].inBattleground ? (mod.game.me.inBattleground || mod.game.me.zone === 152) : !mod.game.me.inBattleground)) {
                    useItem(hpPot[hp]);
                    hpPot[hp][1].inCd = true;
                    hpPot[hp][1].amount--;
                    setTimeout(function() {
                        hpPot[hp][1].inCd = false;
                    }, hpPot[hp][1].cd * 1000);
                    mod.command.message('Использовано: '.clr('E0FFFF') + (`${hpPot[hp][1].name} `).clr('DC143C') + (` | `).clr('E0FFFF') + (`Осталось `).clr('E0FFFF') + (`${(hpPot[hp][1].amount.toLocaleString())} `).clr('228B22') + (` шт.`).clr('E0FFFF'));
                }
            }
        }
    }

    function useMP(nowMP) {
        if (config.mp && (mod.game.isIngame && !mod.game.isInLoadingScreen && !aRes && mod.game.me.alive && !mod.game.me.mounted)) {
            for (let mp = 0; mp < mpPot.length; mp++) {
                if (!mpPot[mp][1].inCd && mpPot[mp][1].amount > 0 && nowMP <= mpPot[mp][1].use_at && (mpPot[mp][1].inCombat ? mod.game.me.inCombat : true) && (mpPot[mp][1].inBattleground ? (mod.game.me.inBattleground || mod.game.me.zone === 152) : !mod.game.me.inBattleground)) {
                    useItem(mpPot[mp]);
                    mpPot[mp][1].inCd = true;
                    mpPot[mp][1].amount--;
                    setTimeout(function() {
                        mpPot[mp][1].inCd = false;
                    }, mpPot[mp][1].cd * 1000);
                    mod.command.message('Использовано: '.clr('E0FFFF') + (`${mpPot[mp][1].name} `).clr('4169E1') + (` | `).clr('E0FFFF') + (`Осталось `).clr('E0FFFF') + (`${(mpPot[mp][1].amount.toLocaleString())} `).clr('228B22') + (` шт.`).clr('E0FFFF'));
                }
            }
        }
    }

    function useItem(itemId) {
        mod.send('C_USE_ITEM', 3, {
            gameId: mod.game.me.gameId,
            id: s2n(itemId[0]),
            dbid: 0,
            target: 0,
            amount: 1,
            dest: {
                x: 0,
                y: 0,
                z: 0
            },
            loc: aLoc,
            w: wLoc,
            unk1: 0,
            unk2: 0,
            unk3: 0,
            unk4: true
        });
    }

    function getConfig() {
        let data = {};
        try {
            data = jsonRequire('./config.json');
        } catch (e) {
            data = {
                enabled: true,
                hp: true,
                mp: true,
                slaying: false,
                notice: true,
                delayafterRes: 2000
            }
            jsonSave('config.json', data);
        }
        return data;
    }

    function getHP() {
        let data = {};
        try {
            data = jsonRequire('./hp.json');
        } catch (e) {
            data = {
                "6000": {
                    "inBattleground": false,
                    "use_at": 80,
                    "cd": 60,
                    "inCombat": false,
                    "slay_at": 10,
                    "name": "Зелье лечения I"
                },
                "6001": {
                    "inBattleground": false,
                    "use_at": 65,
                    "cd": 10,
                    "inCombat": false,
                    "slay_at": 10,
                    "name": "Большое зелье лечения I"
                },
                "6002": {
                    "inBattleground": false,
                    "use_at": 80,
                    "cd": 60,
                    "inCombat": false,
                    "slay_at": 10,
                    "name": "Зелье лечения II"
                },
                "6003": {
                    "inBattleground": false,
                    "use_at": 65,
                    "cd": 10,
                    "inCombat": false,
                    "slay_at": 10,
                    "name": "Большое зелье лечения II"
                },
                "6004": {
                    "inBattleground": false,
                    "use_at": 80,
                    "cd": 60,
                    "inCombat": false,
                    "slay_at": 10,
                    "name": "Зелье лечения III"
                },
                "6005": {
                    "inBattleground": false,
                    "use_at": 65,
                    "cd": 10,
                    "inCombat": false,
                    "slay_at": 10,
                    "name": "Большое зелье лечения III"
                },
                "6006": {
                    "inBattleground": false,
                    "use_at": 80,
                    "cd": 60,
                    "inCombat": false,
                    "slay_at": 10,
                    "name": "Зелье лечения IV"
                },
                "6007": {
                    "inBattleground": false,
                    "use_at": 65,
                    "cd": 10,
                    "inCombat": false,
                    "slay_at": 10,
                    "name": "Большое зелье лечения IV"
                },
                "6008": {
                    "inBattleground": false,
                    "use_at": 80,
                    "cd": 60,
                    "inCombat": false,
                    "slay_at": 10,
                    "name": "Зелье лечения V"
                },
                "6009": {
                    "inBattleground": false,
                    "use_at": 65,
                    "cd": 10,
                    "inCombat": false,
                    "slay_at": 10,
                    "name": "Большое зелье лечения V"
                },
                "6010": {
                    "inBattleground": false,
                    "use_at": 80,
                    "cd": 60,
                    "inCombat": false,
                    "slay_at": 10,
                    "name": "Зелье лечения VI"
                },
                "6011": {
                    "inBattleground": false,
                    "use_at": 65,
                    "cd": 10,
                    "inCombat": false,
                    "slay_at": 10,
                    "name": "Большое зелье лечения VI"
                },
                "6012": {
                    "inBattleground": false,
                    "use_at": 80,
                    "cd": 60,
                    "inCombat": false,
                    "slay_at": 10,
                    "name": "Зелье лечения VII"
                },
                "6013": {
                    "inBattleground": false,
                    "use_at": 65,
                    "cd": 10,
                    "inCombat": false,
                    "slay_at": 10,
                    "name": "Большое зелье лечения VII"
                },
                "6014": {
                    "inBattleground": false,
                    "use_at": 80,
                    "cd": 60,
                    "inCombat": false,
                    "slay_at": 10,
                    "name": "Зелье лечения VIII"
                },
                "6015": {
                    "inBattleground": false,
                    "use_at": 65,
                    "cd": 60,
                    "inCombat": false,
                    "slay_at": 10,
                    "name": "Большое зелье лечения VIII"
                },
                "6048": {
                    "inBattleground": false,
                    "use_at": 80,
                    "cd": 60,
                    "inCombat": false,
                    "slay_at": 10,
                    "name": "Лечащий эликсир I"
                },
                "6049": {
                    "inBattleground": false,
                    "use_at": 65,
                    "cd": 10,
                    "inCombat": false,
                    "slay_at": 10,
                    "name": "Крепкий лечащий эликсир I"
                },
                "6050": {
                    "inBattleground": false,
                    "use_at": 80,
                    "cd": 60,
                    "inCombat": false,
                    "slay_at": 10,
                    "name": "Лечащий эликсир II"
                },
                "6051": {
                    "inBattleground": false,
                    "use_at": 65,
                    "cd": 10,
                    "inCombat": false,
                    "slay_at": 10,
                    "name": "Крепкий лечащий эликсир II"
                },
                "6052": {
                    "inBattleground": false,
                    "use_at": 80,
                    "cd": 60,
                    "inCombat": false,
                    "slay_at": 10,
                    "name": "Лечащий эликсир III"
                },
                "6053": {
                    "inBattleground": false,
                    "use_at": 65,
                    "cd": 10,
                    "inCombat": false,
                    "slay_at": 10,
                    "name": "Крепкий лечащий эликсир III"
                },
                "6054": {
                    "inBattleground": false,
                    "use_at": 80,
                    "cd": 60,
                    "inCombat": false,
                    "slay_at": 10,
                    "name": "Лечащий эликсир IV"
                },
                "6055": {
                    "inBattleground": false,
                    "use_at": 65,
                    "cd": 10,
                    "inCombat": false,
                    "slay_at": 10,
                    "name": "Крепкий лечащий эликсир IV"
                },
                "6056": {
                    "inBattleground": false,
                    "use_at": 80,
                    "cd": 60,
                    "inCombat": false,
                    "slay_at": 10,
                    "name": "Лечащий эликсир V"
                },
                "6057": {
                    "inBattleground": false,
                    "use_at": 65,
                    "cd": 10,
                    "inCombat": false,
                    "slay_at": 10,
                    "name": "Крепкий лечащий эликсир V"
                },
                "6058": {
                    "inBattleground": false,
                    "use_at": 80,
                    "cd": 60,
                    "inCombat": false,
                    "slay_at": 10,
                    "name": "Лечащий эликсир VI"
                },
                "6059": {
                    "inBattleground": false,
                    "use_at": 65,
                    "cd": 10,
                    "inCombat": false,
                    "slay_at": 10,
                    "name": "Крепкий лечащий эликсир VI"
                },
                "6060": {
                    "inBattleground": false,
                    "use_at": 80,
                    "cd": 60,
                    "inCombat": false,
                    "slay_at": 10,
                    "name": "Лечащий эликсир VII"
                },
                "6061": {
                    "inBattleground": false,
                    "use_at": 65,
                    "cd": 10,
                    "inCombat": false,
                    "slay_at": 10,
                    "name": "Крепкий лечащий эликсир VII"
                },
                "6062": {
                    "inBattleground": false,
                    "use_at": 80,
                    "cd": 60,
                    "inCombat": false,
                    "slay_at": 10,
                    "name": "Лечащий эликсир VIII"
                },
                "6063": {
                    "inBattleground": false,
                    "use_at": 65,
                    "cd": 10,
                    "inCombat": false,
                    "slay_at": 10,
                    "name": "Крепкий лечащий эликсир VIII"
                },
                "6300": {
                    "inBattleground": false,
                    "use_at": 80,
                    "cd": 60,
                    "inCombat": false,
                    "slay_at": 10,
                    "name": "Лечащий эликсир IX"
                },
                "6480": {
                    "inBattleground": false,
                    "use_at": 65,
                    "cd": 60,
                    "inCombat": false,
                    "slay_at": 10,
                    "name": "Крепкий лечащий эликсир X"
                },
                "6550": {
                    "inBattleground": false,
                    "use_at": 80,
                    "cd": 10,
                    "inCombat": false,
                    "slay_at": 10,
                    "name": "Зелье HP низкого уровня"
                },
                "6551": {
                    "inBattleground": false,
                    "use_at": 65,
                    "cd": 10,
                    "inCombat": false,
                    "slay_at": 10,
                    "name": "Зелье HP среднего уровня"
                },
                "6552": {
                    "inBattleground": false,
                    "use_at": 80,
                    "cd": 10,
                    "inCombat": false,
                    "slay_at": 10,
                    "name": "Зелье HP высшего уровня"
                },
                "6553": {
                    "inBattleground": false,
                    "use_at": 65,
                    "cd": 10,
                    "inCombat": false,
                    "slay_at": 10,
                    "name": "Зелье HP самого высокого уровня"
                },
                "6560": {
                    "hp": false,
                    "use_at": 70,
                    "cd": 10,
                    "inCombat": false,
                    "slay_at": 10,
                    "name": "Зелье MP низкого уровня"
                },
                "6561": {
                    "hp": false,
                    "use_at": 65,
                    "cd": 10,
                    "inCombat": false,
                    "slay_at": 10,
                    "name": "Зелье MP среднего уровня"
                },
                "6562": {
                    "hp": false,
                    "use_at": 50,
                    "cd": 10,
                    "inCombat": false,
                    "slay_at": 10,
                    "name": "Зелье MP высшего уровня"
                },
                "6563": {
                    "hp": false,
                    "use_at": 50,
                    "cd": 10,
                    "inCombat": false,
                    "slay_at": 10,
                    "name": "Зелье MP самого высокого уровня"
                },
                "111": {
                    "inBattleground": false,
                    "use_at": 18,
                    "cd": 30,
                    "inCombat": false,
                    "slay_at": 10,
                    "name": "Зелье обновления 75%"
                },
                "112": {
                    "inBattleground": false,
                    "use_at": 18,
                    "cd": 30,
                    "inCombat": false,
                    "slay_at": 10,
                    "name": "Зелье лечения 75%"
                },
                "114": {
                    "inBattleground": false,
                    "use_at": 35,
                    "cd": 30,
                    "inCombat": false,
                    "slay_at": 10,
                    "name": "Валькионское зелье здоровья 50%"
                },
                "116": {
                    "inBattleground": false,
                    "use_at": 35,
                    "cd": 30,
                    "inCombat": false,
                    "slay_at": 10,
                    "name": "Зелье здоровья"
                },
                "149644": {
                    "inBattleground": false,
                    "use_at": 18,
                    "cd": 30,
                    "inCombat": false,
                    "slay_at": 10,
                    "name": "Припасы Эссении: Зелье обновления 75%"
                },
                "207289": {
                    "inBattleground": false,
                    "use_at": 40,
                    "cd": 30,
                    "inCombat": false,
                    "slay_at": 10,
                    "name": "Припасы Эссении: Эликсир"
                },
                "207462": {
                    "inBattleground": false,
                    "use_at": 40,
                    "cd": 30,
                    "inCombat": false,
                    "slay_at": 10,
                    "name": "Припасы Эссении: Эликсир"
                }
            }
            jsonSave('hp.json', data);
        }
        return jsonSort(data, 'use_at');
    }

    function getMP() {
        let data = {};
        try {
            data = jsonRequire('./mp.json');
        } catch (e) {
            data[6562] = {
                name: 'Prime Replenishment Potable',
                inBattleground: false,
                inCombat: false,
                use_at: 50,
                cd: 10
            }
            jsonSave('mp.json', data);
        }
        return jsonSort(data, 'use_at');
    }

    function s2n(n) {
        return Number(n);
    }

    function msg(msg) {
        cmd.message(msg);
    }

    function jsonRequire(data) {
        delete require.cache[require.resolve(data)];
        return require(data);
    }

    function jsonSort(data, sortby) {
        let key = Object.keys(data).sort(function(a, b) {
            return parseFloat(data[b][sortby]) - parseFloat(data[a][sortby])
        });
        let s2a = [];
        for (let i = 0; i < key.length; i++) s2a.push([key[i], data[key[i]]]);
        return s2a;
    }

    function jsonSave(name, data) {
        fs.writeFile(path.join(__dirname, name), JSON.stringify(data, null, 4), err => {});
    }

    function TFString(e) {
        return `<font color="${TFColor(e)}" size="+20">${(e ? 'True' : 'False')}</font><br>`;
    }

    function TFColor(e) {
        return e ? '#4DE19C' : '#FE6F5E';
    }
}