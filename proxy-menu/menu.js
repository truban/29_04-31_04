/* eslint-disable quote-props,  comma-dangle */
"use strict";

// Цвета
const c = {
	"w": "#ffffff", // белый
	"br": "#bc8f8f", // коричневый
	"o": "#daa520", // оранжевый
	"p": "#ed5d92", // розовый
	"lp": "#ffb7c5", // светло-розовый
	"r": "#fe6f5e", // красный
	"g": "#4de19c", // зеленый
	"lg": "#5bc0be", // светло-зеленый
	"v": "#9966cc", // фиолетовый
	"lv": "#e0b0ff", // светло-фиолетовый
	"b": "#436eee", // синий
	"lb": "#08b3e5", // светло-синий
	"gr": "#778899", // серый
	"y": "#c0b94d", // желтый
	"pr": "#800080" , // пурпурный
	"m": "#ff00ff" , // Фуксия
	
	"black": "#000000" , // Чёрный (Black)
	"navy_blue": "#000080" , // Тёмно-синий (Navy blue)
	"blue": "#0000ff" , // Синий (Blue)
	"green": "#008000" , // Зеленый (Green)
	"teal": "#008080" , // Сине-зелёный (Teal)
	"lime": "#00ff00" , // Лайм (Lime)
	"cyan": "#00ffff" , // Морская волна (Cyan)
	"maroon": "#800000" , // Тёмно-бардовый (Maroon)
	"purple": "#800080" , // Пурпурный (Purple)
	"olive": "#808000" , // Оливковый (Olive)
	"gray": "#808080" , // Серый (Gray)
	"silver": "#c0c0c0" , // Серебряный (Silver)
	"red": "#ff0000" , // Красный (Red)
	"magenta": "#ff00ff" , // Фуксия (Magenta)
	"yellow": "#ffff00" , // Жёлтый (Yellow)
	"white": "#ffffff" , // Белый (White)
	"orange": "#daa520", // оранжевый
	"orangered": "#FF4500", // оранжево красный
};

// Дочступные ключи записи:
//   name    -- Название пункта меню
//   color   -- Цвет пункта меню
//   keybind -- Установка горячей клавиши
//   ifcmd   -- Фильтр (отображение) пункта меню, если указанная команда найдена
//   ifnocmd -- Фильтр (отображение) пункта меню, если указанная команда не найдена
//   class   -- Фильтр (отображение) пункта меню по игровому классу:
//                  warrior, lancer, slayer, berserker, sorcerer, archer, priest,
//                  elementalist, soulless, engineer, fighter, assassin, glaiver
//
// Встроенный команды:
//   mm et  [quest] [instance] -- Телепортация по Авангарду
//   mm use [id предмета]      -- Использовать предмет из инвентаря

// Настройка премиум-слотов
module.exports.premium = [
    { command: "m", id: 20000008 }, //Очки доблести
    { command: "ab", id: 461 }, //Продавец снаряжения альянса
	{ command: "bank", id: 60264 },
	{ command: "broker", id: 60265 },
	{ command: "store", id: 60262 },
	{ command: "pot hp", id: 6479 }, //HP
	{ command: "pot mp", id: 6079 }, //MP	
	{ command: "loot", id: 70068 } //Мусор
];

// Настройка меню
module.exports.categories = {
	"Основное": [
		{ command: "bank", name: "Банк", color: c.magenta },
		{ command: "gbank", name: "Банк гильдии", color: c.magenta },
		{ command: "ab", name: "Автобанк", color: c.magenta },
		{ command: "gat", name: "Сбор", color: c.cyan },
		{},
		{ command: "pot", name: "Автоюз банки", color: c.yellow },
		{ command: "loot auto", name: "Автолут", color: c.yellow },
		{ command: "cc", name: "Антиопрокид", color: c.yellow },
		{ command: "autoheal", name: "Автохил", color: c.yellow },
		{ command: "heal", name: "Auto-Target", color: c.yellow },
		{ command: "bb", name: "Anti-Bodyblock", color: c.yellow },
		{},
		{ command: "fwc", name: "FWC Guide", color: c.yellow },
		{ command: "mute", name: "Блок Чата", color: c.yellow },
		{},
		{ command: "warn relay", name: "Monster Marker", color: c.orange },
		{ command: "mobid", name: "MobId-Finder", color: c.orange },
		{ command: "item", name: "ItemID", color: c.orange },
		{},
		{ command: "ach", name: "Трекер Достижений", color: c.green },
		{ command: "goldinfo", name: "Инф. по золоту в час", color: c.green },
		{ command: "vq", name: "'Задания Авангарда", color: c.green },
		{ command: "math", name: "'Блок чата", color: c.green },
		{},
		{ command: "win", name: "Кто с Вами рядом", color: c.green },
		{ command: "gm", name: "Обнаруживает GM", color: c.green },
		{},
		{ command: "m premium", name: "VIP панель", color: c.red },
	],	
	"Торговцы": [	
		{ command: "broker", name: "Брокер", color: c.lime },
		{ command: "store", name: "Торговец", color: c.teal },
		{ command: "sstore", name: "Магазин редкостей", color: c.teal },
		{ command: "vg", name: "Торговец авангарда", color: c.teal },
		{},
		{ command: "godkaya", name: "Богиня Кайа", color: c.orange },
		{ command: "godvelika", name: "Богиня Велика", color: c.orange },
	],
    "Торговцы для Крафта": [	
		{ command: "matmakarmor", name: "Мат. для доспехов", color: c.br },
		{ command: "drawmakarmor", name: "Черт. для доспехов", color: c.br },
		{ command: "drawweapons", name: "Черт. для оружия", color: c.br },
		{ command: "matweapon", name: "Мат. для оружия", color: c.br },
		{},
		{ command: "alchformulas", name: "Алх. формулы", color: c.cyan },
		{ command: "alchmaterials", name: "Алх. материалы", color: c.cyan },
		{ command: "matengraving", name: "Материалы для гравировки", color: c.cyan },
		{ command: "pattengraving", name: "Узоры гравировки", color: c.cyan },
	],	
	"Тонкие настройки Модов": [
		{ command: "m $ach", name: "Трекер Достижений", color: c.orangered },
		{ command: "m $gather", name: "Cбор ресурсов", color: c.orangered },
		{ command: "m $mmarker", name: "Monster Marker", color: c.orangered },
		{ command: "m $autoheal", name: "Авто Хил-Таргет", color: c.orangered },
		{ command: "m $mute", name: "Блок Чата", color: c.orangered },
		{},
		{ command: "m $Loger", name: "PacketsLogger", color: c.orangered }
	],			
};

module.exports.pages = {
	ach: {
		"Трекер Достижений": [
			{ command: "ach", name: "Вкл./Откл. Модуль", color: c.o },
			{},
			{ command: "ach alert", name: "Вкл./Откл. Уведомление в центре", color: c.o },
			{},
			{ command: "ach status", name: "Состояние Модуля", color: c.o },	
		]
	},
	gather: {
		"Подсказки для сбора ресурсов": [
			{ command: "gat", name: "Вкл./Откл. Модуль", color: c.o },
			{},
			{ command: "gat alert", name: "Вкл./Откл. Уведомление в центре", color: c.o },
			{},
			{ command: "gat plants", name: "Вкл./Откл. Подсказки по растениям", color: c.o },
			{},
			{ command: "gat ore", name: "Вкл./Откл. Подсказки по руде", color: c.o },
			{},
			{ command: "gat energy", name: "Вкл./Откл. Подсказки по энергии", color: c.o },
			{},
			{ command: "gat status", name: "Состояние Модуля", color: c.o }
			
		]
	},
	mmarker: {
		"Monster Marker": [
			{ command: "warn relay", name: "Вкл./Откл. Модуль", color: c.r },
			{},
			{ command: "warn alert", name: "Вкл./Откл. Уведомление в центре", color: c.p },
			{},
			{ command: "warn marker", name: "Вкл./Откл. Маркеры", color: c.r },
			{},
			{ command: "warn clear", name: "Сброс меток", color: c.lr },
			{},
			{ command: "warn", name: "Информация", color: c.lr }
		],
		"MobId-Finder": [
			{ command: "mobid", name: "Вкл./Откл. Модуль", color: c.o },
			{},
			{ command: "mism", name: "Вкл./Откл. Показ ID в чате", color: c.o }		
		]
	},
	mute: {
		"Запрет писать в канал чата": [
			{ command: "mute", name: "Вкл./Откл. Модуль", color: c.r },
			{},
			{ command: "mute add 0", name: "Say", color: c.p },
			{ command: "mute add 1", name: "Party", color: c.r },
			{ command: "mute add 2", name: "Guild", color: c.lr },
			{ command: "mute add 3", name: "Area", color: c.lr },
			{ command: "mute add 4", name: "Trade", color: c.lr },
			{ command: "mute add 27", name: "Global", color: c.lr },
			],
		"Очиска заблокированных каналов": [
			{ command: "mute remove 1", name: "Party", color: c.r },
			{ command: "mute remove 2", name: "Guild", color: c.lr },
			{ command: "mute remove 3", name: "Area", color: c.lr },
			{ command: "mute remove 4", name: "Trade", color: c.lr },
			{ command: "mute remove 27", name: "Global", color: c.lr },
			{},
            { command: "mute clean", name: "Очистка блока чата", color: c.r },
			{ command: "mute list", name: "Список блок чатов", color: c.r },
			{ command: "mute info", name: "Инфо", color: c.r }
		]
	},
	autoheal: {
		"Автохил": [
			{ command: "autoheal", name: "Вкл./Откл. Модуль", color: c.o },
			{},
			{ command: "autoheal 50", name: "Лечить с 30% HP", color: c.o },
			{},
			{ command: "autoheal 50", name: "Лечить с 50% HP", color: c.o },
			{},
			{ command: "autoheal 50", name: "Лечить с 70% HP", color: c.o },
			{},
			{ command: "autoheal 100", name: "Лечить всегда", color: c.o },
			{},
			{ command: "autocleanse", name: "Вкл./Откл. Auto-Cleansing", color: c.o },
			{},
			{ command: "autocast", name: "Вкл./Откл. Auto-Casting", color: c.o }	
		],
		"Auto Target": [
			{ command: "heal", name: "Вкл./Откл. Модуль", color: c.o },
			{},
			{ command: "heal auto", name: "Вкл./Откл. Auto-Heal", color: c.o },
			{},
			{ command: "heal dispell", name: "Вкл./Откл. Auto-Cleansing", color: c.o },
			{},
			{ command: "heal dps", name: "Вкл./Откл. Auto-DPS", color: c.o },
			{},
			{ command: "heal cast", name: "Вкл./Откл. Auto-Casting", color: c.o },
			{},
			{ command: "heal status", name: "Статус Модуля", color: c.o },
			{},
			{ command: "heal debug", name: "Вкл./Откл. Дебаг", color: c.o },
			{},
			{ command: "heal test", name: "Вкл./Откл. Тест", color: c.o }
		]
	},	
	Loger: {
		Mods: [
			{ command: "proxy reload packetslogger", name: "Logger reload", color: c.p, ifcmd: "logs" },
			{ ifcmd: "logs" },
			{ command: "logc", name: "LOG C", color: c.bl, ifcmd: "logc" },
			{ command: "logs", name: "LOG S", color: c.o, ifcmd: "logs" }
		],
		"Смена Каналов": [
			{ command: "ch 1", name: "(_1_)", color: c.g },
			{ command: "ch 2", name: "(_2_)", color: c.g },
			{ command: "ch 3", name: "(_3_)", color: c.g },
			{ command: "ch 4", name: "(_4_)", color: c.g },
			{ command: "ch 5", name: "(_5_)", color: c.lv },
			{ command: "ch 6", name: "(_6_)", color: c.lv },
			{ command: "ch 7", name: "(_7_)", color: c.lb },
			{ command: "ch 8", name: "(_8_)", color: c.lb },
			{ command: "ch 9", name: "(_9_)", color: c.b },
			{ command: "ch 10", name: "(_10_)", color: c.b }
		]
	},
	setting: {
		Настройки: [
			{ command: "m premium", name: "Доп. кнопки VIP панели", color: c.y }
		]
	}
};