'use strict';
String.prototype.clr = function(hexColor) {
    return `<font color='#${hexColor}'>${this}</font>`;
};
module.exports = function antiBodyBlock(mod) {
    const partyMembers = new Set();
    const cache = Object.create(null);
    const partyObj = Object.create(null);

    let interval = null;
    let enabled = true;

    partyObj.raid = true;

    const removeBodyBlock = () => {
        for (let i = partyMembers.values(), step; !(step = i.next()).done;) {
            partyObj.gameId = step.value;
            partyObj.partyId = cache.partyId;
            mod.send("S_PARTY_INFO", 1, partyObj);
        }
    };

    mod.game.on('enter_game', () => {
        if (enabled) {
            interval = mod.setInterval(removeBodyBlock, 5000);
        }
    });

    mod.command.add("bb", () => {
        enabled = !enabled;
        mod.command.message('Модуль: ' + (s.enabled ? 'Включен'.clr('56B4E9') : 'Отключен'.clr('FF0000')) + '.');
        if (enabled) {
            interval = mod.setInterval(removeBodyBlock, 5000);
        } else {
            mod.clearInterval(interval);
        }
    });

    mod.hook("S_PARTY_INFO", 1, evt => {
        Object.assign(cache, evt);
    });
    mod.hook("S_PARTY_MEMBER_LIST", 7, evt => {
        partyMembers.clear();
        for (let i = 0, arr = evt.members, len = arr.length; i < len; ++i) {
            const member = arr[i];
            if (!member.online) continue;
            partyMembers.add(member.gameId);
        }
    });
};