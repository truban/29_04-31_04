'use strict';
String.prototype.clr = function(hexColor) {
    return `<font color='#${hexColor}'>${this}</font>`;
};

// const path = require('path'),
	  // fs = require('fs')
//Defaults
let sysmsg=false, //display system messages for ids (Default false)
	enabled=false //default enable/disable module

module.exports = function mobidfinder(mod) {

	let idlist=new Map()

	mod.command.add('mobid', () => {
			enabled = !enabled
			if(!enabled) idlist.clear()
		    mod.command.message('Модуль: ' + (enabled ? 'Включен. Убейте моба, и ID будет отображен в консоле или чате.'.clr('56B4E9') : 'Отключен'.clr('FF0000')));
	})

	mod.command.add('mism', () => {
			sysmsg = !sysmsg
			mod.command.message('Показ ID в чате: ' + (sysmsg ? 'Включен'.clr('56B4E9') : 'Отключен'.clr('FF0000')));
	})

	mod.hook('S_SPAWN_NPC', 11, event => {
		if(enabled) idlist.set(event.gameId,`${event.huntingZoneId}_${event.templateId}`);
	})

	mod.hook('S_DESPAWN_NPC', 3,event => {
		if(enabled && event.type===5) {
			if(idlist.has(event.gameId)) {
				console.log(`Monster id (huntingZoneId_templateId): ${idlist.get(event.gameId)}`)
				if(sysmsg) mod.command.message(`Monster id (huntingZoneId_templateId): ${idlist.get(event.gameId)}`)
				idlist.delete(event.gameId)
			}
		}
	})

	mod.hook('S_LOAD_TOPO', "raw",event => {
		idlist.clear()
	})
}
