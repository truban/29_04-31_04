Original Author Seren, Updated and upkept by Witch

## Commands

/8 mobid | Toggle module on/off - to stop spamming your console. (Disabled by Default)

/8 mism | Toggle the MobID's from showing in-game chat. (Disabled by Default)


# Mob-id-finder
Easy way to find a mob's huntingZoneId and templateId by killing the mob

Requires Commands module: https://gitlab.com/truban/29_04-31_04/-/tree/main/command


## Instruction
After making sure this module is enabled, load the target mob onto your screen and then kill it (strongly suggest you only hit that target mob). The killed mob huntingZoneId and templateId ids will be displayed on the console (command prompt) by default.

----
 
### Команды интегрированы в модуль прокси-меню

### Commands are integrated into the proxy menu module
