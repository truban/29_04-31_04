'use strict'
// Item IDs that have unique glows
//
// 91116   Amarun's Relic Piece     //Avatar
// 55658   Bahaar's Relic Piece     //Avatar
// 91166   Dagon's Relic Piece      //Avatar
// 91114   Elinu's Relic Piece      //Avatar
// 91118   Gidd's Relic Piece       //Avatar
// 91177   Ishara's Relic Piece     //Avatar
// 91113   Isren's Relic Piece      //Avatar
// 91119   Karas's Relic Piece      //Avatar
// 91188   Oriyn's Relic Piece      //Avatar
// 57000   Seren's Relic Piece      //Avatar
// 91115   Tithus's Relic Piece     //Avatar
// 91117   Zuras's Relic Piece      //Avatar
// 98260   Vergos's Head            // Vergo pieces all have the same effect
// 98263   Vergos's Horn
// 98262   Vergos's Scale
// 98264   Vergos's Bone
// 98261   Vergos's Fang
// 440 649 6515 8000-8024 45411 91113
// 88704 // Памятная монета Велики

/*
Reference List
HuntingZoneIDs: Bluebox-1023 | Caiman-1023 | crabs-6553782 | mongos seems to be dependent on location, are the zone ids the same as orignal location?
Template IDs: Bluebox-88888888 | Caiman-99999999,99999991,99999992 | crabs-1021 | unknown for mongos | Test-mob - 181_2023

To discover more ids, hook S_SPAWN_NPC and check huntingzoneid and templateId. Or use 'mob-id-finder' module on my Github (SerenTera)

Configs are in config.json. If you do not have it, it will be auto generated on your first login
*/

const path = require('path'),
	  fs = require('fs')

module.exports = function markmob(mod) {
	
	const Message = require('../tera-message');
	const MSG = new Message(mod);

	let	mobid=[],
		config,
		fileopen = true,
		stopwrite,
		enabled,
		active = false,
		markenabled,
		messager,
		alerts,
		markerId,
		Monster_ID,
		specialMobSearch

	try{
		config = JSON.parse(fs.readFileSync(path.join(__dirname,'config.json'), 'utf8'))
		let defaultConfig = JSON.parse(fs.readFileSync(path.join(__dirname,'lib','configDefault.json'), 'utf8'))
		if(config.gameVersion !== defaultConfig.gameVersion || config.entriesVersion != defaultConfig.gameVersion && config.allowAutoEntryRemoval) {
			let oldMonsterList = JSON.parse(JSON.stringify(config.Monster_ID)), //Deep Clone to replace new list with old config using shallow merge
				newMonsterEntry = JSON.parse(JSON.stringify(defaultConfig.newEntries))

			if(config.allowAutoEntryRemoval === undefined) {
				console.log('[Monster Marker] A new config option (allowAutoEntryRemoval) is added to allow this module to automatically clear old event monster entries. It is by default enabled, and you have to disable it in config.json before next login if you do not want this.');
			}
			else if(config.allowAutoEntryRemoval) {
				for(let key of defaultConfig.deleteEntries) {	//Delete old unused entries for events that are over using deleteEntries
					if(oldMonsterList[key]) {
						console.log(`[Monster Marker] Removed old event entry: ${oldMonsterList[key]}`)
						delete oldMonsterList[key]
					}
				}
				config.entriesVersion = defaultConfig.gameVersion
			}

			Object.assign(oldMonsterList,newMonsterEntry) //Remember to remove the newentries for every update as well as remove old entries from past event

			config = Object.assign({},defaultConfig,config,{gameVersion:defaultConfig.gameVersion,Monster_ID:oldMonsterList}) //shallow merge
			delete config.newEntries
			delete config.deleteEntries
			save(config,'config.json')
			console.log('[Monster Marker] Updated new config file. Current settings transferred over.')
		}
		configInit()
	}
	catch(e){
		let defaultConfig = JSON.parse(fs.readFileSync(path.join(__dirname,'lib','configDefault.json'), 'utf8'))
		config = defaultConfig
		Object.assign(config.Monster_ID,config.newEntries)
		delete config.newEntries
		delete config.deleteEntries
		config.entriesVersion = defaultConfig.gameVersion
		save(config,'config.json')
		configInit()
		console.log('[Monster Marker] New config file generated. Settings in config.json.')
	}


///////Commands
	mod.command.add('warn', {
		$default() {
			mod.command.message('Инфо. Напишите "warn info","warn relay","warn alert","warn marker","warn clear","warn active","warn add", для помощи')
		},

		info() {
			mod.command.message(`Version: ${config.gameVersion}`)
			mod.command.message('Commands: warn [arguments]\nArguments are as follows:\nrelay: Enable/Disable Module\nalert: Toggle popup alerts\nmarker: Toggles markers spawn\nclear: Clears marker\nactive: Checks if module is active zone\nadd huntingZone templateId name: Adds the entry to the config')
		},

		relay() {
			enabled=!enabled
			MSG.chat("Модуль: " + (enabled ? MSG.BLU("Включен") : MSG.RED("Отключен")));

			if(!enabled)
				for(let itemid of mobid) despawnthis(itemid)
		},

		alert() {
			alerts = !alerts
			MSG.chat("Всплывающее уведомление системы: " + (alerts ? MSG.BLU("Включено") : MSG.RED("Отключено")));
		},

		marker() {
			markenabled = !markenabled
			mod.command.message(markenabled ? 'Маркеры элементов включены' : 'Маркеры элементов отключены')
			MSG.chat("Маркеры элементов: " + (markenabled ? MSG.BLU("Включено") : MSG.RED("Отключено")));
		},

		clear() {
			mod.command.message('Попытки сброса меток предметов')
			for(let itemid of mobid) despawnthis(itemid)
		},

		active() {
			MSG.chat("Маркеры элементов: " + MSG.BLU("${active}"));
		},

		add(huntingZone,templateId,name) {
			config.Monster_ID[`${huntingZone}_${templateId}`] = name
			Monster_ID[`${huntingZone}_${templateId}`] = name
			save(config,'config.json')
			mod.command.message(` Добавлена Запись Конфигурации: ${huntingZone}_${templateId}= ${name}`)
		}

	})

////////Dispatches
	mod.hook("S_SPAWN_NPC", 11, event => {	//Use version >5. Hunting zone ids are indeed only int16 types.
		if(!active || !enabled) return
		
		if(Monster_ID[`${event.huntingZoneId}_${event.templateId}`]) {
			if(markenabled) {
				markthis(event.loc,event.gameId*100n), //create unique id ?
				mobid.push(event.gameId)
			}

			if(alerts) notice(MSG.RED("Обнаружен: ") + MSG.YEL(Monster_ID[`${event.huntingZoneId}_${event.templateId}`]))
			if(messager) MSG.chat(MSG.RED("Обнаружен: ") + MSG.YEL(Monster_ID[`${event.huntingZoneId}_${event.templateId}`]))		
		}

		else if(specialMobSearch && event.bySpawnEvent) { //New def
			if(markenabled) {
				markthis(event.loc,event.gameId*100n),
				mobid.push(event.gameId)
			}
			if(alerts) notice('Обнаружен Ивентовый Монстр')
			if(messager) mod.command.message('Обнаружен Ивентовый Монстр')
		}

	})

	mod.hook('S_DESPAWN_NPC', 3, event => {
		if(mobid.includes(event.gameId)) {
			despawnthis(event.gameId*100n),
			mobid.splice(mobid.indexOf(event.gameId), 1)
		}
	})

	mod.hook('S_LOAD_TOPO', 3, event => { //reset mobid list on location change
		mobid=[]
		active = event.zone < 9000  //Check if it is a dungeon instance, since event mobs can come from dungeon
	})


////////Functions
	function markthis(loc,idRef) {
        loc.z -= 100;		
		mod.send('S_SPAWN_DROPITEM', 6, {
			gameId: idRef,
			loc: loc,
			item: 45411, // Маркер: Каменный леденец кумаса
			amount: 1,
			expiry: 30000,
			explode: false,
			masterwork: false,
			enchant: 0,
			owners: [{id: 0}]
		})
	}
	
	function despawnthis(despawnid) {
		mod.send('S_DESPAWN_DROPITEM', 4, {
			gameId: despawnid
		})
	}

	function notice(msg) {
		mod.send('S_CHAT', 2, {
            channel: 21,
            message: msg
        })
    }

	function save(data,args) {
		if(!Array.isArray(args)) args = [args] //Find a way around this later -.-

		if(fileopen) {
			fileopen=false
			fs.writeFile(path.join(__dirname, ...args), JSON.stringify(data,null,"\t"), err => {
				if(err) mod.command.message('Ошибка записи файла, попытка перезаписи')
				fileopen = true
			})
		}
		else {
			clearTimeout(stopwrite)			 //if file still being written
			stopwrite=setTimeout(save(__dirname,...args),2000)
			return
		}
	}

	function configInit() {
		({enabled,markenabled,messager,alerts,markerId,Monster_ID,specialMobSearch} = config)
	}
}
