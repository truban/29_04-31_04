'use strict';
String.prototype.clr = function(hexColor) {
    return `<font color='#${hexColor}'>${this}</font>`;
};

module.exports = function Vanguardian(mod) {
    const path = require('path');
    mod.dispatch.addDefinition("S_COMPLETE_EVENT_MATCHING_QUEST", 1, path.join(__dirname, "defs", "S_COMPLETE_EVENT_MATCHING_QUEST.1.def"));
    mod.dispatch.addDefinition("S_AVAILABLE_EVENT_MATCHING_LIST", 1, path.join(__dirname, "defs", "S_AVAILABLE_EVENT_MATCHING_LIST.1.def"));
    mod.dispatch.addDefinition("C_COMPLETE_DAILY_EVENT", 1, path.join(__dirname, "defs", "C_COMPLETE_DAILY_EVENT.1.def"));
    mod.dispatch.addDefinition("C_COMPLETE_EXTRA_EVENT", 1, path.join(__dirname, "defs", "C_COMPLETE_EXTRA_EVENT.1.def"));

    let timeout = null,
        daily = 0,
        weekly = 0,
        finishedQuests = [],
        niceName = mod.proxyAuthor !== 'caali' ? '[VG] ' : ''

    // ############# //
    // ### Hooks ### //
    // ############# //

    mod.game.on('enter_game', () => {
        daily = weekly = 0
        timeout = null
    })

    mod.hook('S_COMPLETE_EVENT_MATCHING_QUEST', 1, event => {
        daily++
        weekly++
        if (!mod.settings.enabled) return
        finishedQuests.push(event.id)
        timeout = setTimeout(() => {
            CompleteQuest()
        }, 2000)
        return false
    })

    mod.hookOnce('S_AVAILABLE_EVENT_MATCHING_LIST', 1, event => {
        daily = event.unk4
        weekly = event.unk6
    })

    // ################# //
    // ### Functions ### //
    // ################# //

    function CompleteQuest() {
        clearTimeout(timeout)
        if (!mod.settings.enabled) return
        if (mod.game.me.alive && !mod.game.me.inBattleground) {
            if (finishedQuests.length) {
                for (let id of finishedQuests) {
                    mod.toServer('C_COMPLETE_DAILY_EVENT', 1, {
                        id
                    })
                    if (daily == 3 || daily == 8) setTimeout(() => {
                        CompleteExtra(1)
                    }, 1000)
                    if (weekly == 16) setTimeout(() => {
                        CompleteExtra(0)
                    }, 1500)
                }
                finishedQuests = []
            }
            report()
        } else timeout = setTimeout(() => {
            CompleteQuest()
        }, 5000)
    }

    function CompleteExtra(type) {
        if (mod.settings.enabled) mod.toServer('C_COMPLETE_EXTRA_EVENT', 1, {
            type
        })
    }

    function report() {
        if (daily < 16) mod.command.message((`${niceName}`).clr('E0FFFF') + (`Выполнено ежедневных заданий Авангарда: `).clr('FFD700') + (`${daily}`).clr('FFA500'));
        else mod.command.message((niceName + 'Сегодня вы выполнили все 16 заданий Авангарда..').clr('FF4500'));
        if (daily < 16) sendAlert(('Выполнено ежедневных заданий Авангарда: ').clr('FFD700') + (`${daily}`).clr('FF8C00'));
        else sendAlert(('СЕГОДНЯ ВЫ ВЫПОЛНИЛИ ВСЕ').clr('FF4500') + (' 16 ').clr('FFD700') + ('ЗАДАНИЙ АВАНГАРДА..').clr('FF4500'))
    }

    function sendAlert(msg) {
        mod.send('S_CHAT', 2, {
            channel: 21,
            chat: false,
            message: msg,
        });
    }

    // ################ //
    // ### Commands ### //
    // ################ //

    mod.command.add('vq', (param) => {
        if (param == null) {
            mod.settings.enabled = !mod.settings.enabled
            mod.command.message(('Модуль ').clr('FFD700') + (`${niceName}`).clr('E0FFFF') + (mod.settings.enabled ? 'Включен'.clr('56B4E9') : 'Отключен'.clr('FF0000')))
            // console.log('Vanguardian ' + (mod.settings.enabled ? 'enabled' : 'disabled'))
        } else if (param == "daily") report()
        else mod.command.message('Commands:\n' +
            ' "vq" (enable/disable Vanguardian),\n' +
            ' "vq daily" (Сообщает Вам, сколько заданий Авангарда вы выполнили сегодня.")'
        )
    })
}