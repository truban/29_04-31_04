'use strict';
String.prototype.clr = function(hexColor) {
    return `<font color='#${hexColor}'>${this}</font>`;
};

const fs = require('fs');
const path = require('path');
const folderName = '=GMLogger=';
const directoryPath = path.join(__dirname, '..', '..', '=GMLogger=');

// Получаем текущую дату и время
const currentDate = new Date();
const formattedDate = currentDate.toISOString().replace(/:/g, '-').slice(0, 19);
// Создаем имя файла, включающее дату и время
const fileName = `file_${formattedDate}.log`;

// Получаем полный путь к файлу
const filePath = path.join(__dirname, '..', '..', '=GMLogger=', 'GM-logger');

fs.mkdir(directoryPath, (err) => {
    if (err) {
        // console.error(`Не удалось создать папку: ${err} так как она существует, игнорируем эту ошибку.`);
    } else {
        console.log(`Папка "${folderName}" успешно создана в ${directoryPath}`);
    }
});

module.exports = function GmDetector(mod) {

    let file = path.join(__dirname, '..', '..', '=GMLogger=', 'GM-logger' + `${formattedDate}` + '.log');
    let enabled = true;

    mod.hook('S_SPAWN_USER', 14, event => {
        if (enabled) {
            if (event.gm) {
                let message = '[GM] ' + event.name + ' Обнаружен!';
                mod.command.message('[GM] '.clr('FF0000') + (`${event.name} `).clr('FFD700') + ("Guild:").clr('2E8B57') + (` ${e.guildName} `).clr('228B22') + (' ОБНАРУЖЕН!').clr('FF0000'));
                if (event.gmInvisible) message += ' (В Инвизе)';
                fs.appendFileSync(file, `=[GM]= Name: ${e.name} / Guild: ${e.guildName}\n`);

                mod.toClient('S_CHAT', 1, {
                    channel: 21,
                    name: 'GM-Detector',
                    message: message
                });
                command.message(msg);

                event.gmInvisible = false; // показать скрытых GM?
                return true;
            }
        }
    });

    mod.command.add("gm", () => {
        enabled = !enabled
        mod.command.message('Модуль: ' + (enabled ? 'Включен'.clr('56B4E9') : 'Отключен'.clr('FF0000')) + '.');
    })
}