module.exports = function findItemID(mod) {
    const command = mod.command;

    mod.command.add(['item'], (arg1, arg2) => {
        if (arg1 && arg1.length > 0) arg1 = arg1.toLowerCase();
        if (arg2 && arg2.length > 0) arg2 = arg2.toLowerCase();
        switch (arg1) {
            case 'id':
                arg2 = arg2 ? arg2.match(/#(\d*)@/) : 0;
                mod.command.message(`ID Предмета: ${(arg2 ? Number(arg2[1]) : 0)}.`);
                console.log(`ID Предмета: ${(arg2 ? Number(arg2[1]) : 0)}.`);
                break;
            default:
                mod.command.message('В чате прокси Введите: '.clr('FF8C00') + (`item id `).clr('FF4500') + (` Ctrl+ПКМ по предмету`).clr('FF8C00'));
                break;

        }
    });

    function msg(msg) {
        command.message(msg);
    }
}