# Who-is-near 
## _Кто с Вами рядом_
------

<p align="center"><img src="https://raw.githubusercontent.com/war100ck/TeraToolbox-modifications-v.100.02/main/Who-is-near/screen/Who-is-near.png" alt="Your image title" width="1000"/>

------
# Описание
-  Выводит в игровой чат и консоль кто находится с Вами рядом, и сохраняет в логфайл.

