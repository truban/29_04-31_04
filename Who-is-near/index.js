﻿'use strict';
String.prototype.clr = function(hexColor) {
    return `<font color='#${hexColor}'>${this}</font>`;
};

const fs = require('fs');
const path = require('path');
const folderName = '=PlayerLogger=';
const directoryPath = path.join(__dirname, '..', '..', '=PlayerLogger=');

// Получаем текущую дату и время
const currentDate = new Date();
const formattedDate = currentDate.toISOString().replace(/:/g, '-').slice(0, 19);
// Создаем имя файла, включающее дату и время
const fileName = `file_${formattedDate}.log`;

// Получаем полный путь к файлу
const filePath = path.join(__dirname, '..', '..', '=PlayerLogger=', 'player-logger');

fs.mkdir(directoryPath, (err) => {
    if (err) {
        // console.error(`Не удалось создать папку: ${err} так как она существует, игнорируем эту ошибку.`);
    } else {
        console.log(`Папка "${folderName}" успешно создана в ${directoryPath}`);
    }
});

module.exports = function PlayerLogger(mod) {

    let c = mod.command;
    let s = mod.settings;

    let file = path.join(__dirname, '..', '..', '=Playerlogger=', 'player-logger' + `${formattedDate}` + '.log');
    let enabled = true;
    mod.hook('S_SPAWN_USER', 14, e => {
		//console.log(e);
        if (s.enable) {
            mod.command.message('Рядом с Вами Игрок: '.clr('E0FFFF') + (`${e.name} `).clr('FFD700') + (` ${e.level} `).clr('56B4E9') + ("LvL ").clr('56B4E9') + ("Guild:").clr('2E8B57') + (` ${e.guildName} `).clr('228B22') + ("GuildRank:").clr('2E8B57') + (` ${e.guildRank}`).clr('228B22'));
            console.log(`Рядом с Вами Игрок:  ${e.name} - ${e.level} - ${e.guildName}`);
            fs.appendFileSync(file, `Name: ==${e.name}== / ${e.level} LvL / Guild: ${e.guildName} / GuildRank: ${e.guildRank}\n`);
        }
    });

    c.add("win", () => {
        s.enable = !s.enable;
        mod.command.message('Модуль: ' + (s.enable ? 'Включен'.clr('56B4E9') : 'Отключен'.clr('FF0000')) + '.');
    })
}
