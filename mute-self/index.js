String.prototype.clr = function(hexColor) {
    return `<font color='#${hexColor}'>${this}</font>`;
};

module.exports = function MuteSelf(mod) {
	const command = mod.command || mod.require.command;

	mod.command.add(['math', 'mute', 'muteself'], {
		$none() {
			mod.settings.enabled = !mod.settings.enabled;
			mod.command.message('Модуль: ' + (mod.settings.enabled ? 'Включен'.clr('56B4E9') : 'Отключен'.clr('FF0000')));
		},
		add(channel) {
			channel = Number(channel);
			let message;
			if (!mod.settings.channels.includes(channel)) { 
				mod.settings.channels.push(channel)
				mod.command.message('Канал чата: '.clr('FF8C00') + (`[${channel}] `).clr('FF6347') + (`Заблокирован`).clr('FF4500'));
			} else {
				mod.command.message('Канал чата: '.clr('FF8C00') + (`[${channel}] `).clr('FF6347') + (`Уже заблокирован`).clr('FF4500'));
			}
			command.message(message);
		},
		remove(channel) {
			channel = Number(channel);
			if (mod.settings.channels.includes(channel)) {
				mod.settings.channels.splice(mod.settings.channels.indexOf(channel), 1);
				mod.command.message('Канал чата: '.clr('FF8C00') + (`[${channel}] `).clr('FF6347') + ('болше не заблокирован').clr('FF4500'));
			}
		},
		clean() {
			mod.settings.channels = [];
			mod.command.message('Список заблокированных каналов чата очищен: '.clr('FF8C00') + ('Очищен').clr('FF6347'));
		},
		clear() {
			mod.settings.channels = [];
			mod.command.message('Список заблокированных каналов чата очищен: '.clr('FF8C00') + ('Очищен').clr('FF6347'));
		},
		list() {
			mod.command.message(`[ ${mod.settings.channels} ]`);
		},
		info() {
			command.message(`[Мир-0] [Группа-1] [Гильдия-2] [Область-3] [Торговля-4] [Global-27]`);
			command.message(`[Party Notice-21] [Raid Notice-25] [Command?-22] [Whisper-7]`);
			command.message(`[Private Channel-11~18] [Bargain-19] [Announcement?-24]`);
			command.message(`[Megaphone-213] [Guild Advt-214] [Emote-26] [Greet-9] [Fishing Greet-10]`);
		},
		help() {
			command.message(`[Мир-0] [Группа-1] [Гильдия-2] [Область-3] [Торговля-4] [Global-27]`);
			command.message(`[Party Notice-21] [Raid Notice-25] [Command?-22] [Whisper-7]`);
			command.message(`[Private Channel-11~18] [Bargain-19] [Announcement?-24]`);
			command.message(`[Megaphone-213] [Guild Advt-214] [Emote-26] [Greet-9] [Fishing Greet-10]`);
		}
	});

    mod.hook('C_CHAT', 1, { order: -100 }, (event) => {
        if (mod.settings.enabled && mod.settings.channels.includes(event.channel)) {
            if(event.channel != 10) command.message(`<font color="#FF0000">Вы запретили писать на этом канале [${event.channel}] чата</font>`); // non-fishing greet
            return false;
        }
    });
}