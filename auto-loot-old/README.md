# auto-loot-old 
tera-toolbox module to loot items automatically

## Использование/Команды

Комманда | Описание
| ------------- | ------------- |
| loot | Вкл./Откл. Модуль
| loot auto | Toggle automatic loot attempt on interval on/off
| loot set delay <num> | Set delay between loot attempt to `num`
| loot set interval <num> | Set loot delay between automatic loot to `num`

------

## Info
- Original author : [Saegusae](https://github.com/Saegusae)

## Changelog
<details>

</details>