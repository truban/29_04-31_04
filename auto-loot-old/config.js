module.exports = {
    enable: true,
    enableAuto: true,
    lootDelay: 400,
    loopInterval: 300,

    blacklist: [
	    7200, 7201, 7202, 7203, 7204, 7205, 7206, 7207, 7208, 7209, 7210, 7211, 7212, 7213,  // Bomb 
        7214, // Scroll of Resurrection
        8000, // Rejuvenation Mote
        8001, // HP Recovery Mote
        8002, // MP Replenishment Mote
        8003, // Spirited MP Replenishment Mote
        8004, // Strong Resistance Mote
        8005, // Healing Mote
        8008, 8009, 8010, 8011, 8012, 8013, 8014, 8015, 8016, 8017, 8018, 8019, 8020, 8021, 8022, // Arun's Vitae I-XV Mote
        8023, // Arun's Tear Mote
        //8025, // Keening Dawn Mote
        91344, // Fashion Coupon		 
        139113, 166718, 213026, // 행운의 상자 (K TERA)
        169886, 169887, 169888, 169889, 169890, 169891 // Locked ???? Strongbox
    ]
}