## Auto-Target-Heal

## Описание:
Подсветка рессурсов с выводом сообщений, как всех, так и по отдельности.

------

## Использование/Команды

Комманда | Описание
| ------------- | ------------- |
| heal | Вкл./Откл. Модуль
| heal auto | Вкл./Откл. Auto-Heal
| heal dispell | Вкл./Откл. Auto-Cleansing
| heal dps | Вкл./Откл. Auto-DPS
| heal cast | Вкл./Откл. Auto-Casting
| heal debug | Вкл./Откл. Дебаг

------  
  
### Команды интегрированы в модуль прокси-меню

### Commands are integrated into the proxy menu module
